<?php

// Make sure a session is available (to store the geoip info)
if( !isset($_SESSION) )
{
    session_start();
}

// Load the config and all libraries
require_once('config.php');
require_once('lib/Tracker.php');

// Create the database connection
try {
    $trackerDb = new PDO(
        $config['dsn'],
        $config['username'],
        $config['password'], 
        array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
        )
    );
    $trackerDb->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );


} catch (PDOException $e) {
    echo "ERROR: ". $e->getMessage();
}
