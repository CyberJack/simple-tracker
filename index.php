<?php

require_once('bootstrap.php');
require_once('lib/Visitors.php');
$visitors = new Visitors( $trackerDb );

// Show/Hide bots?
$showBots = ( isset($_GET['bots']) && "true" === $_GET['bots'] ? true : false );

// Build the data array
$data = array(
    'showBots' => $showBots,
    'title' => 'Last 50 visits',
    'records' => $visitors->getLatestVisitors( 50, $showBots )
);

// Display
require_once('templates/layout.php');
