<?php

// Bootstrap the tracker
require_once('bootstrap.php');

// No private addresses
if( !preg_match( '/^(0|127|172|10|192)\./', Tracker::getRealIp() ) && !in_array( Tracker::getRealIp(), $config['exclude'] ) )
{
    // Load the Browser library
    require_once('lib/Browser.php');

    // Get the database record id's for the browser and geoip
    $browserId = Tracker::storeBrowser( $trackerDb, new Browser() );
    $geoIpId = Tracker::storeGeoIP( $trackerDb );

    // Insert a new visit record
    Tracker::storeVisit( $trackerDb, $browserId, $geoIpId );
}
