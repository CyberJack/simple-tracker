Simple Visitor Tracker
======================
A simple visitor tracker.

Requirements
------------
- PHP 5.4
- PHP with Curl support
- PHP with PDO MySQL support

Install
-------
- create a MySQL database and import the db/tracker.sql file
- copy the "config.php.dist" to "config.php" and change the settings

Usage
-----
- require the "tracker.php" file in your project.
