<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Simple Visitor Tracker">
    <meta name="author" content="Simple Visitor Tracker">
    <link rel="icon" href="favicon.ico">

    <title>Simple Visitor Tracker</title>

    <!-- CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/flags.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body role="document">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Simple Visitor Tracker</a>
            </div>
        </div>
    </nav>

    <div class="container theme-showcase" role="main">
        <div class="page-header">
            <h3><?php echo $data['title'] ?></h3>
<?php if( true === $data['showBots']): ?>
            <a class="robotbtn" href="index.php?bots=false"><button type="button" class="btn btn-sm btn-danger">Hide robots</button></a>
<?php else: ?>
            <a class="robotbtn" href="index.php?bots=true"><button type="button" class="btn btn-sm btn-success">Show robots</button></a>
<?php endif; ?>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Browser</th>
                            <th>Geo Data</th>
                            <th>Requests</th>
                        </tr>
                    </thead>
                    <tbody>
<?php foreach( $data['records'] AS $row): ?>
                        <tr class="<?php echo ('Y' === $row['browser']['robot'] ? 'robot' : 'browser'); ?>">
                            <td style="width:30%;">
                                <table class="table table-striped table-hover table-bordered">
                                    <tr>
                                        <td style="width: 25%;">Name</td>
                                        <td data-toggle="tooltip" data-placement="bottom" title="<?php echo $row['browser']['agent']; ?>"><?php echo $visitors->getBrowserIcon( $row['browser']['name'] ); ?><?php echo $row['browser']['name']; ?></td>
                                    </tr>
                                    <tr><td>Version</td><td><?php echo $row['browser']['version']; ?></td></tr>
                                    <tr>
                                        <td>Platform</td>
                                        <td><?php echo $visitors->getOsIcon( $row['browser']['platform'] ); ?><?php echo $row['browser']['platform']; ?></td>
                                    </tr>
                                    <tr><td>Mobile</td><td><?php echo ( 'Y' === $row['browser']['mobile'] ? 'Yes' : 'No' ); ?></td></tr>
                                    <tr><td>Tablet</td><td><?php echo ( 'Y' === $row['browser']['tablet'] ? 'Yes' : 'No' ); ?></td></tr>
                                    <tr><td>Robot</td><td><?php echo ( 'Y' === $row['browser']['robot'] ? 'Yes' : 'No' ); ?></td></tr>
                                </table>
                            </td>
                            <td style="width:30%;">
                                <table class="table table-striped table-hover table-bordered">
                                    <tr><td style="width: 25%;">IP</td><td><?php echo $row['geoip']['ip']; ?></td></tr>
                                    <tr><td>Country</td><td><span class="flag flag-<?php echo strtolower($row['geoip']['country_code3']); ?>" id="TKM"></span>&nbsp;<?php echo $row['geoip']['country']; ?></td></tr>
                                    <tr><td>Region</td><td><?php echo $row['geoip']['region']; ?></td></tr>
                                    <tr><td>City</td><td><?php echo $row['geoip']['city']; ?></td></tr>
                                    <tr><td>ISP</td><td><?php echo $row['geoip']['isp']; ?></td></tr>
                                    <tr><td>Maps</td><td><a href="http://maps.google.com/maps?q=<?php echo $row['geoip']['latitude']; ?>,<?php echo $row['geoip']['longitude']; ?>" target="_blank">Click here</a></td></tr>
                                </table>
                            </td>
                            <td style="width: 40%;">
                                <table class="table table-striped table-hover table-bordered">
    <?php foreach( $row['visits'] AS $v ): ?>
                                    <tr><td style="width: 25%;"><?php echo $v['time']->format("d-m-Y H:i:s"); ?></td><td><?php echo $v['uri']; ?></td></tr>
    <?php endforeach; ?>    
                                </table>
                            </td>
                        </tr>
<?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
</body>
</html>

