<?php

class Tracker
{
    /**
     * Get the users IP address
     *
     * @return string
     */
    public static function getRealIp()
    {
        // Init
        $ip = '0.0.0.0';

        // Check the ip
        $toCheck = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'REMOTE_ADDR');
        foreach( $toCheck as $check )
        {
            if( isset( $_SERVER[ $check ] ) && !empty( $_SERVER[ $check ] ) )
            {
                $ip = $_SERVER[ $check ];
                break;
            }
        }

        // Return
        return $ip;
    }

    /**
     * Store the browser in the database
     *
     * @param PDO $db
     * @param Browser $browser
     * @return int
     */
    public static function storeBrowser(PDO $db, Browser $browser)
    {
        // First check if the browser is already in the database
        $sth = $db->prepare("SELECT id FROM browser WHERE agent = :agent");
        $sth->bindValue( ':agent', $browser->getUserAgent() );
        $sth->execute();

        $result = $sth->fetch(PDO::FETCH_ASSOC);
        if( !$result )
        {
            // Insert new browser into the database
            $sth = $db->prepare("INSERT INTO browser (`agent`,`name`,`version`,`platform`,`mobile`,`tablet`,`robot`) VALUES (:agent, :name, :version, :platform, :mobile, :tablet, :robot)");
            $sth->bindValue( ":agent", $browser->getUserAgent() );
            $sth->bindValue( ":name", $browser->getBrowser() );
            $sth->bindValue( ":version", $browser->getVersion() );
            $sth->bindValue( ":platform", $browser->getPlatform() );
            $sth->bindValue( ":mobile", ( $browser->isMobile() ? 'Y' : 'N' ) );
            $sth->bindValue( ":tablet", ( $browser->isTablet() ? 'Y' : 'N' ) );
            $sth->bindValue( ":robot", ( $browser->isRobot() ? 'Y' : 'N' ) );
            $sth->execute();
            $ret = $db->lastInsertId();
        } else {
            // Result found
            $ret = $result['id'];
        }

        // Return
        return $ret;
    }

    /**
     * Store the browser in the database
     *
     * @param PDO $db
     * @return int
     */
    public static function storeGeoIp(PDO $db )
    {
        $geoip = self::getGeoIP($db);
        if( null === $geoip['dbId'] )
        {
            // New database record
            $sth = $db->prepare("
                INSERT INTO geoip (`ip`,`country_code`,`country_code3`,`country`,`region_code`,`region`,`city`,`postal_code`,`continent_code`,`latitude`,`longitude`,`dma_code`,`area_code`,`asn`,`isp`,`timezone`,`offset`,`updated`)
                VALUES (:ip, :country_code, :country_code3, :country, :region_code, :region, :city, :postal_code, :continent_code, :latitude, :longitude, :dma_code, :area_code, :asn, :isp, :timezone, :offset, NOW())
            ");
            $sth->bindValue( ":ip", $geoip['ip'] );
            $sth->bindValue( ":country_code", $geoip['country_code'] );
            $sth->bindValue( ":country_code3", $geoip['country_code3'] );
            $sth->bindValue( ":country", $geoip['country'] ); 
            $sth->bindValue( ":region_code", $geoip['region_code'] ); 
            $sth->bindValue( ":region", $geoip['region'] ); 
            $sth->bindValue( ":city", $geoip['city'] ); 
            $sth->bindValue( ":postal_code", $geoip['postal_code'] ); 
            $sth->bindValue( ":continent_code", $geoip['continent_code'] ); 
            $sth->bindValue( ":latitude", $geoip['latitude'] ); 
            $sth->bindValue( ":longitude", $geoip['longitude'] ); 
            $sth->bindValue( ":dma_code", $geoip['dma_code'] ); 
            $sth->bindValue( ":area_code", $geoip['area_code'] ); 
            $sth->bindValue( ":asn", $geoip['asn'] );
            $sth->bindValue( ":isp", $geoip['isp'] );
            $sth->bindValue( ":timezone", $geoip['timezone'] );
            $sth->bindValue( ":offset", $geoip['offset'] );
            $sth->execute();
            $_SESSION['trackerGeoIP']['dbId'] = $db->lastInsertId();
        }
        return $_SESSION['trackerGeoIP']['dbId'];
    }
    
    /**
     * Try to fetch the visitors GeoIP data
     *
     * @return array
     */
    public static function getGeoIP(PDO $db)
    {
        // Init
        if( !array_key_exists('trackerGeoIP', $_SESSION) )
        {
            // Init the record
            $record = array(
                'dbId' => null,             // The database record ID
                'ip' => null,               // (Visitor IP address, or IP address specified as parameter)
                'country_code' => null,     // (Two-letter ISO 3166-1 alpha-2 country code)
                'country_code3' => null,    // (Three-letter ISO 3166-1 alpha-3 country code)
                'country' => null,          // (Name of the country)
                'region_code' => null,      // (Two-letter ISO-3166-2 state / region code)
                'region' => null,           // (Name of the region)
                'city' => null,             // (Name of the city)
                'postal_code' => null,      // (Postal code / Zip code)
                'continent_code' => null,   // (Two-letter continent code)
                'latitude' => null,         // (Latitude)
                'longitude' => null,        // (Longitude)
                'dma_code' => null,         // (DMA Code)
                'area_code' => null,        // (Area Code)
                'asn' => null,              // (Autonomous System Number)
                'isp' => null,              // (Internet service provider)
                'timezone' => null,         // (Time Zone)
                'offset' => null,           // ??
            );

            // First check of there is a geoip record from within the last 30 days
            $sth = $db->prepare("
                SELECT
                    *
                FROM
                    geoip g
                WHERE
                    g.ip = :ip 
                    AND g.updated >= DATE_SUB(SYSDATE(), INTERVAL 30 DAY)");
            $sth->bindValue( ':ip', self::getRealIp() );
            $sth->execute();

            $result = $sth->fetch(PDO::FETCH_ASSOC);
            if( !$result )
            {
                // No existing GeoIP data found,
                // try to fetch new info from 
                $url = 'http://www.telize.com/geoip/'. self::getRealIp();
            
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    // Disable SSL verification
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     // Will return the response, if false it print the response
                curl_setopt($ch, CURLOPT_URL, $url);
                $curlResult = curl_exec( $ch );
                curl_close( $ch );

                // Fill the blanks
                if( $curlResult )
                {
                    foreach( json_decode( $curlResult ) AS $key => $value ) 
                    {
                        $record[ $key ] = $value;
                    }
                }
            }
            else
            {
                // Record from database
                foreach( $result AS $key => $value )
                {
                    if( $key === 'id' )
                        $key = 'dbId';

                    $record[ $key ] = $value;
                }
            }

            // Add record to the session
            $_SESSION['trackerGeoIP'] = $record;
        }
        
        // Return
        return $_SESSION['trackerGeoIP'];
    }

    /**
     * Store the visit in the database
     *
     * @param PDO $dbA
     * @param int $browserId
     * @param int $geoIpId
     * @return void
     */
    public static function storeVisit(PDO $db, $browserId, $geoIpId )
    {
        // Build the full url
        $url = '';
        if( isset( $_SERVER ) )
        {
            $url = sprintf(
                "%s://%s%s",
                $_SERVER['REQUEST_SCHEME'],
                $_SERVER['SERVER_NAME'],
                $_SERVER['REQUEST_URI']
            );
        }

        // Store the visit
        $sth = $db->prepare("INSERT INTO visit (`browser_id`,`geoip_id`,`time`,`uri`) VALUES (:browser_id, :geoip_id, NOW(), :uri)");
        $sth->bindValue( ":browser_id", $browserId );
        $sth->bindValue( ":geoip_id", $geoIpId );
        $sth->bindValue( ":uri", $url ); 
        $sth->execute();
    }
}
