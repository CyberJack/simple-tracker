<?php

class Visitors
{
    /**
     * The database object
     * @var PDO
     */
    protected $db;


    /**
     * Constructor
     */
    public function __construct(PDO $db)
    {
        $this->db = $db;
    }


    /**
     * Get the latest visitors
     *
     * @param int $limit
     * @param bool $showBots
     * @return array
     */
    public function getLatestVisitors($limit=50, $showBots=false)
    {
        // Init
        $ret = array();

        // Show bots
        $in = array('N');
        if( true === $showBots )
            $in[] = 'Y';
        $inQuery = implode(',', array_map( function($val) { return '"'. $val .'"'; }, $in) );

        // Get the visitors
        $sth = $this->db->prepare("
            SELECT 
                v.browser_id,
                v.geoip_id
            FROM
                visit v
            INNER JOIN
                browser b ON (b.id = v.browser_id)
            WHERE
                b.robot IN (". $inQuery .")
            GROUP BY
                v.geoip_id
            ORDER BY
                v.time DESC
            LIMIT 0, ". (int)$limit);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);

        // Loop all the visitors to get all the information
        foreach( $result AS $row )
        {
            $ret[] = $this->fetchVisit( $row );
        }

        // Return
        return $ret;
    }

    /**
     * Fetch all data for a single visit
     *
     * @param array $row
     * @return array
     */
    private function fetchVisit( array $row )
    {
        // Init
        return array(
            'browser' => $this->fetchBrowserData( $row['browser_id'] ),
            'geoip' => $this->fetchGeoIpData( $row['geoip_id'] ),
            'visits' => $this->fetchVisitData( $row['browser_id'], $row['geoip_id'] )
        );
    }

    /**
     * Fetch browser data
     *
     * @param int $id
     * @return array|null
     */
    private function fetchBrowserData( $id )
    {
        $sth = $this->db->prepare("SELECT * FROM browser b WHERE b.id = :id");
        $sth->bindValue( ":id", $id, PDO::PARAM_INT );
        $sth->execute();
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        return( $result ? $result : null );
    }

    /**
     * Fetch geoip data
     *
     * @param int $id
     * @return array|null
     */
    private function fetchGeoIpData( $id )
    {
        $sth = $this->db->prepare("SELECT * FROM geoip g WHERE g.id = :id");
        $sth->bindValue( ":id", $id, PDO::PARAM_INT );
        $sth->execute();
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        return( $result ? $result : null );
    }

    /**
     * Fetch geoip data
     *
     * @param int $browserId
     * @param int $geoipId
     * @return array|null
     */
    private function fetchVisitData( $browserId, $geoipId )
    {
        // Init 
        $ret = array();

        // Fetch all the visit records
        $sth = $this->db->prepare("SELECT v.time, v.uri FROM visit v WHERE v.browser_id = :browser_id AND v.geoip_id = :geoip_id ORDER BY v.time DESC LIMIT 25");
        $sth->bindValue( ":browser_id", $browserId, PDO::PARAM_INT );
        $sth->bindValue( ":geoip_id", $geoipId, PDO::PARAM_INT );
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);

        foreach( $result AS $row )
        {
            $row['time'] = DateTime::createFromFormat( 'Y-m-d H:i:s', $row['time'] );
            $ret[] = $row;
        }

        // Return
        return ( $ret ? $ret : null );
    }

    /**
     * Get browser icon (used in templates)
     *
     * @param string $browser
     * @return string
     */
    public function getBrowserIcon( $browser )
    {
        // Init
        $template = '<span class="browsers browsers-%s"></span>';

        // Check browser (where icons are available)
        if( preg_match('/^(Chrome|Chomium|Safari|Firefox|Opera|Internet Explorer)$/i', $browser, $matches) )
        {
            return sprintf($template, strtolower( $matches[1] ));
        }

        // Not found
        return '';
    }

    /**
     * Get os icon (used in templates)
     *
     * @param string $platform
     * @return string
     */
    public function getOsIcon( $platform )
    {
        // Init
        $template = '<span class="os os-%s"></span>';

        // Check browser (where icons are available)
        if( preg_match('/^(Android|Apple|Ipod|Iphone|iPad|Win|Windows|Linux)$/i', $platform, $matches) )
        {
            switch( strtolower( $matches[1] ) )
            {
                case 'apple':
                case 'ipod':
                case 'ipad':
                case 'iphone':
                        return sprintf($template, 'apple');
                        break;
                case 'win':
                case 'windows':
                        return sprintf($template, 'windows');
                        break;
            }
            return sprintf($template, strtolower( $matches[1] ) );
        }

        // Not found
        return '';
    }
}
